public class Board
{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board()
	{
		die1 = new Die();
		die2 = new Die();
		tiles = new boolean[12];
	}
	
	public String toString()
	{
		String tilesValue = "";
		for(int i=0, value=1; i <tiles.length; i++)
		{
			if(tiles[i]==false)
			{
				tilesValue += " " + value;
			}
			if(tiles[i]==true)
			{
				tilesValue+= " X";
			}
			value++;
		}
		return tilesValue;
	}
	
	public boolean playATurn()
	{
		die1.roll();
		die2.roll();
		System.out.println("The result of the first die is: " + die1);
		System.out.println("The result of the second die is: " + die2);	
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		if(tiles[sumOfDice-1] == false)
		{
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		
		if(tiles[sumOfDice-1] == true)
		{
			if(tiles[die1.getFaceValue()-1]==false)
			{
				tiles[die1.getFaceValue()-1]=true;
				System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
				return false;
			}
			
			else if(tiles[die2.getFaceValue()-1]==false)
			{
				tiles[die2.getFaceValue()-1]=true;
				System.out.println("Closing tile with the same value as die two: " + die2.getFaceValue());
				return false;
			}
		
			else
			{
				System.out.println("All the tiles for these values are already shut.");
			    return true;
			}
		}
		return true;
	}
			
}