public class Jackpot
{
	public static void main(String[] args)
	{
		System.out.println("Welcome to the game!");
		Board game1 = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		while(gameOver == false)
		{
			System.out.println(game1);
			if(game1.playATurn()==true)
			{
				gameOver = true;
			}
			else
			{
				numOfTilesClosed++;
			}
		}
		if(numOfTilesClosed>=7)
		{
			System.out.println("You have reached JACKPOT. You won!");
		}
		else
		{
			System.out.println("You have not reached JACKPOT. You lose.");
		}
	}
}